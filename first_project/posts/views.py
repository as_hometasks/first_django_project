from django.shortcuts import render
from .models import FirstClass

def index(request):
    return render(request, 'index.html')

def index2_1(request):
    obj = FirstClass.objects.all().first()
    return render(request, 'index2_1.html', {'obj': obj})

def index2_2(request):
    objects = FirstClass.objects.all()
    return render(request, 'index2_2.html', {'objects': objects})
# Create your views here.
